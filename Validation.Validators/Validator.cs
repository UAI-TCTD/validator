﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Validators
{
    
   public abstract class Validator<T>
    {
        protected MyNameValueCollection nvc;
        public Validator()
        {
            nvc = new MyNameValueCollection();
        }

        
        public abstract void Validate(T entity);
        
    }
}
