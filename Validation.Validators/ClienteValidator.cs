﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;

namespace Validation.Validators
{
    public class ClienteValidator : Validator<Cliente>
    {
        public override void Validate(Cliente entity)
        {

            if (entity.Nombre.Length < 3 || entity.Nombre == null)
                nvc.Add("100", "El nombre debe tener mas de 3 caracteres");
            if (entity.Edad < 18)
                nvc.Add("101", "El cliente no puede ser menor de edad");

            if (nvc.Count > 0) throw new MyBusinessException(nvc);
            
        }
    }
}
