﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Validators
{
   public  class MyNameValueCollection : NameValueCollection
    {
        public string GetAllValuesJoined()
        {
            string joint = String.Empty;
            var items = this.AllKeys.SelectMany(this.GetValues, (k, v) => new { key = k, value = v });
            foreach (var item in items)
            {
                joint += string.Format("{0} {1} {2}", item.key, item.value, System.Environment.NewLine);
            }

            return joint;
        }
    }
}
