﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Validators
{
    public class MyBusinessException : Exception
    {
        private MyNameValueCollection _nvc;
        public MyBusinessException(MyNameValueCollection nvc)
        {
            _nvc = nvc;
        }
        public string GetAllErrorsJoined()
        {
            return _nvc.GetAllValuesJoined();
        }
    }
}
