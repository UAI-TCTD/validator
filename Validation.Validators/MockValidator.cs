﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;

namespace Validation.Validators
{
    public class MockValidator<T> : Validator<T>
    {
        public override void Validate(T entity)
        {
         //do nothing...
        }
    }
}
