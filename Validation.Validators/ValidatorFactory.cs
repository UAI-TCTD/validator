﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;
using Validation.Validators;

namespace Validation.Validators
{
    public  class ValidatorFactory
    {
         

       public  Validator<T> GetValidator<T>() 
       {
            Validator<T> validator =null;
            try
            {

              validator = (Validator <T> )Assembly.GetExecutingAssembly().CreateInstance(String.Format("{0}.{1}Validator", this.GetType().Namespace, typeof(T).Name));
                
            }
            catch (Exception)
            {
                //no hacemos nada
            }

            if (validator == null) return new MockValidator<T>();
            return validator;
                
            
            

        }
    }
}
