﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Domain
{
   public interface IDbContext<T> : ICrud<T> where T : Entity
    {
   }
}
