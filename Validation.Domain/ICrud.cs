﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Domain
{
   public interface ICrud<T> where T : Entity
    {
        
       T  Save(T entity);
       void Remove(T entity);
               
       IList<T> GetAll();
        
    }
}
