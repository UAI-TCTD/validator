﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Domain
{
    public abstract class Entity
    {
        public Guid Id { get; set; }



        // Esto es para poder comparar durante el "Contains" de una lista
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        public override bool Equals(object other)
        {
            return Equals(other as Entity);
        }

        public virtual bool Equals(Entity other)
        {
            if (other == null) { return false; }
            if (object.ReferenceEquals(this, other)) { return true; }
            return this.Id == other.Id;
        }

        public static bool operator ==(Entity item1, Entity item2)
        {
            if (object.ReferenceEquals(item1, item2)) { return true; }
            if ((object)item1 == null || (object)item2 == null) { return false; }
            return item1.Id == item2.Id; //cuando quiero comparar dos objetos de tipo Entity, lo hago directamente por id
        }

        public static bool operator !=(Entity item1, Entity item2)
        {
            return !(item1 == item2);
        }

    }
}
