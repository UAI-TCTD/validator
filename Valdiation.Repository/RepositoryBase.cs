﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;

namespace Validation.Repository
{

    public interface IRepository<T> : ICrud<T> where T : Entity
    {

    }

    public abstract class RepositoryBase<T>  : IRepository<T> where T : Entity
    {

        protected IDbContext<T> context;


        //acciones comunes.
        public IList<T> GetAll()
        {
            return context.GetAll();
        }

        public void Remove(T entity)
        {
            context.Remove(entity);
        }

        public T Save(T entity)
        {
            return context.Save(entity);
        }
    }
}
