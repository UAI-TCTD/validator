﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;
using Validation.InMemoryProvider;

namespace Validation.Repository
{

    public interface IClienteRepository : IRepository<Cliente>
    {

    }

    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
        public ClienteRepository()
        {
            base.context = new ClienteContext();
        }
    }
}
