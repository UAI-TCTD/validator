﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;

namespace Validation.InMemoryProvider
{
    public class DBContext<T> : IDbContext<T>  where T:Entity
    {

        protected IList<T> lista;
        public DBContext()
        {
            lista = new List<T>();
        }
        public IList<T> GetAll()
        {
            return lista;
        }

        public void Remove(T entity)
        {
            lista.Remove(entity);
        }

        public T Save(T entity)
        {

            
            if (lista.Contains(entity))
            {
                T item = lista.Where(t => t.Equals(entity)).FirstOrDefault();

                item = entity;
            }
            else
            {
                entity.Id = Guid.NewGuid();
                lista.Add(entity);
            }

            return entity;
            
        }
    }
}
