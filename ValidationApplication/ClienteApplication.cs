﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;
using Validation.Application;
using Validation.Repository;

namespace Validation.Application
{
    public class ClienteApplication : ApplicationBase<Cliente>
    {
        public ClienteApplication()
        {
            repository = new ClienteRepository();
        }
    }
}
