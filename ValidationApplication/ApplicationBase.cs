﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Domain;
using Validation.Validators;

namespace Validation.Application
{
    public interface IApplication<T> : ICrud<T> where T: Entity
    {
        //aca van las operaciones genericas de la bll
        void BeforeSave(T entity);
    }

    public abstract class ApplicationBase<T> : IApplication<T> where T: Entity
    {
        public virtual void BeforeSave(T entity)
        {
            ValidatorFactory validator = new ValidatorFactory();
            validator.GetValidator<T>().Validate(entity);
        }

        protected ICrud<T> repository;
        public IList<T> GetAll()
        {
            return repository.GetAll();
        }

        public void Remove(T entity)
        {
            
            repository.Remove(entity);
        }

        public T Save(T entity)
        {
            BeforeSave(entity);
            return repository.Save(entity);
        }
    }
}
